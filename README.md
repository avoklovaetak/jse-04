# TASK MANAGER

Console application for task list.

# DEVELOPER INFO

NAME: Ekaterina Volkova

E-MAIL: avoklovaetal@yandex.ru

E-MAIL: esvolkova@tsconsulting.com

# SOFTWARE

* JDK 1.8

* Windows

# HARDWARE

* RAM 8Gb

* CPU i5

* HDD 128Gb

# RUN PROGRAM

```
java -jar ./task-manager.jar
```

# SCREENSHOTS

* JSE-00: https://yadi.sk/d/3X352AchCl0Otw?w=1

* JSE-01: https://yadi.sk/d/Ug-Wfq18cjkSKQ?w=1

* JSE-02: https://yadi.sk/d/yDwqBHdS6vMHaw?w=1

* JSE-03: https://yadi.sk/d/8QDSv1DLgN80nw?w=1

* JSE-04: https://yadi.sk/d/XwrnuYT4-rRmUQ
